package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "cbid_esta_cred", schema = "cbid", catalog = "bid")
public class CbidEstaCred {
    private Long idEstaCred;
    private String codEstaProc;
    private String descEstaProc;
    private Integer idEsta;
    private Integer idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @Column(name = "id_esta_cred")
    public Long getIdEstaCred() {
        return idEstaCred;
    }

    public void setIdEstaCred(Long idEstaCred) {
        this.idEstaCred = idEstaCred;
    }

    @Basic
    @Column(name = "cod_esta_proc")
    public String getCodEstaProc() {
        return codEstaProc;
    }

    public void setCodEstaProc(String codEstaProc) {
        this.codEstaProc = codEstaProc;
    }

    @Basic
    @Column(name = "desc_esta_proc")
    public String getDescEstaProc() {
        return descEstaProc;
    }

    public void setDescEstaProc(String descEstaProc) {
        this.descEstaProc = descEstaProc;
    }

    @Basic
    @Column(name = "id_esta")
    public Integer getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(Integer idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public Integer getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Integer idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CbidEstaCred that = (CbidEstaCred) o;
        return Objects.equals(idEstaCred, that.idEstaCred) &&
                Objects.equals(codEstaProc, that.codEstaProc) &&
                Objects.equals(descEstaProc, that.descEstaProc) &&
                Objects.equals(idEsta, that.idEsta) &&
                Objects.equals(idTipo, that.idTipo) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi) &&
                Objects.equals(usrOpeCrea, that.usrOpeCrea) &&
                Objects.equals(usrOpeModi, that.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idEstaCred, codEstaProc, descEstaProc, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
