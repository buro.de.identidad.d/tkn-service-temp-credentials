package com.teknei.bid.persistence.entities;

import javax.persistence.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_rel_cred_temp", schema = "bid", catalog = "bid")
public class BidRelCredTemp {	

    private BigInteger id;
    private BigInteger idClie;
    private BigInteger idCredTemp;
    private Timestamp fchCrea;

    @Id 
    @Column(name="id", unique=true, nullable=false)     
    @SequenceGenerator(sequenceName = "bid.bid_rel_cred_temp_id_seq", name = "temp_id_seq", allocationSize = 1)
    @GeneratedValue(generator = "temp_id_seq", strategy = GenerationType.SEQUENCE)
    public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	@Basic
	@Column(name = "id_clie")
    public BigInteger getIdClie() {
		return idClie;
	}

	public void setIdClie(BigInteger idClie) {
		this.idClie = idClie;
	}

	@Basic
	@Column(name = "id_cred_temp")
	public BigInteger getIdCredTemp() {
		return idCredTemp;
	}

	public void setIdCredTemp(BigInteger idCredTemp) {
		this.idCredTemp = idCredTemp;
	}

	@Basic
	@Column(name = "fch_crea")
	public Timestamp getFchCrea() {
		return fchCrea;
	}

	public void setFchCrea(Timestamp fchCrea) {
		this.fchCrea = fchCrea;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
		BidRelCredTemp bidDisp = (BidRelCredTemp) o;
		return Objects.equals(idClie, bidDisp.idClie) && Objects.equals(idCredTemp, bidDisp.idCredTemp)
				&& Objects.equals(fchCrea, bidDisp.fchCrea);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idClie, idCredTemp,fchCrea);
    }
}
