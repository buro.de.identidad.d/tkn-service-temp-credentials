package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.CbidEstaCred;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CbidEstaCredRepository extends JpaRepository<CbidEstaCred, Long> {

    CbidEstaCred findCbidEstaCredByCodEstaProc(String codEstaProc);

}