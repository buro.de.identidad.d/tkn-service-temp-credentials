package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.BidRelCredTemp;
import com.teknei.bid.persistence.entities.CbidEstaCred;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BidRelCredTempRepository extends JpaRepository<BidRelCredTemp, Long> {
    
	List<BidRelCredTemp> findAllByIdCredTemp(BigInteger idCredTemp);

}