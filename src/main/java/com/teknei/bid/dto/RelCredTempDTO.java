package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

@Data
public class RelCredTempDTO implements Serializable {
	
	private static final long serialVersionUID = -2931008676797516323L;
	
	private BigInteger id;
	private BigInteger idClie;
	private BigInteger idCredTemp;
	private Timestamp fchCrea;

}