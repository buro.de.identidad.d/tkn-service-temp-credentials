package com.teknei.bid.controller.rest;

import com.teknei.bid.controller.rest.service.CredentialsTempService;
import com.teknei.bid.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/credentials")
public class CredentialsTempController {

    @Autowired
    private CredentialsTempService credentialsTempService;
    private BCryptPasswordEncoder passwordEncoder;
    private static final Logger log = LoggerFactory.getLogger(CredentialsTempController.class);

    @PostConstruct
    private void init() {
        passwordEncoder = new BCryptPasswordEncoder();
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Long>> findStatusCred() {
        return new ResponseEntity<>(credentialsTempService.findCredentialStatus(), HttpStatus.OK);
    }

    @RequestMapping(value = "/credentials/{idEstaCred}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CredentialsTempDTO>> findAllByIdStatus(@PathVariable Long idEstaCred) {
        List<CredentialsTempDTO> list = null;
        try {
            list = credentialsTempService.findAllByIdEstaCred(idEstaCred).stream().map(credentialsTempService::entityToDTO).collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Error finding credentials temp list with message: {}", e.getMessage());
        }
        if (list == null || list.isEmpty()) {
            return new ResponseEntity<>(list, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "/credentials", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CredentialsTempDTO>> findAll() {
        List<CredentialsTempDTO> list = null;
        try {
            list = credentialsTempService.findAll().stream().map(credentialsTempService::entityToDTO).collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Error finding credentials temp list with message: {}", e.getMessage());
        }
        if (list == null || list.isEmpty()) {
            return new ResponseEntity<>(list, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "/credentials", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationResultDTO> delete(@RequestBody CredentialsDeleteRequestDTO requestDTO) {
        int status = credentialsTempService.deleteCredentials(requestDTO);
        CredentialsValidationResultDTO resultDTO = new CredentialsValidationResultDTO();
        resultDTO.setStatus(status);
        if (status == 0) {
            resultDTO.setResult(true);
            return new ResponseEntity<>(resultDTO, HttpStatus.OK);
        }
        resultDTO.setResult(false);
        return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @RequestMapping(value = "/credentials", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationResultDTO> create(@RequestBody CredentialsSaveRequestDTO requestDTO) {
        String password = passwordEncoder.encode(requestDTO.getPassword());
        requestDTO.setPassword(password);
        int status = credentialsTempService.generateCredential(requestDTO);
        CredentialsValidationResultDTO resultDTO = new CredentialsValidationResultDTO();
        resultDTO.setStatus(status);
        if (status == 0) {
            resultDTO.setResult(true);
            return new ResponseEntity<>(resultDTO, HttpStatus.OK);
        }
        resultDTO.setResult(false);
        return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @RequestMapping(value = "/credentials", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationResultDTO> validate(@RequestBody CredentialsValidationRequestDTO requestDTO) {
        log.info("XXXXINFO: "+this.getClass().getName()+".{validate() }");
        int result = credentialsTempService.validateCredentials(requestDTO);
        CredentialsValidationResultDTO resultDTO = new CredentialsValidationResultDTO();
        if (result != 0) {
            resultDTO.setResult(false);
            resultDTO.setStatus(result);
            return new ResponseEntity<>(resultDTO, HttpStatus.FORBIDDEN);
        } else {
            resultDTO.setResult(true);
            resultDTO.setStatus(0);
            return new ResponseEntity<>(resultDTO, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/credentials/modify", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationResultDTO> updateCredTemp(@RequestBody UpdateTempStatusCredRequestDTO requestDTO) {
        Boolean result = credentialsTempService.updateCredStatus(requestDTO);
        CredentialsValidationResultDTO resultDTO = new CredentialsValidationResultDTO();
        if (result) {
            resultDTO.setStatus(0);
            resultDTO.setResult(true);
            return new ResponseEntity<>(resultDTO, HttpStatus.OK);
        }
        resultDTO.setResult(false);
        resultDTO.setStatus(1);
        return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @RequestMapping(value = "/credentials/fingers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationResultDTO> validateNoFingers(@RequestBody CredentialsValidationFingersRequestDTO requestDTO) {
        CredentialsValidationResultDTO resultDTO = new CredentialsValidationResultDTO();
        try {
            Integer noFingers = credentialsTempService.validateNoFingersByCredentials(requestDTO);
            resultDTO.setResult(true);
            resultDTO.setStatus(noFingers);
            return new ResponseEntity<>(resultDTO, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            log.error("Unable to retrieve fingers number with message: {}", e.getMessage());
            resultDTO.setStatus(0);
            resultDTO.setResult(false);
            return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/credentials/data", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonalDataCredentialDTO> validateUsername(@RequestBody CredentialsValidationRequestDTO requestDTO) {
        PersonalDataCredentialDTO personalDataCredentialDTO = new PersonalDataCredentialDTO();
        try {
            personalDataCredentialDTO = credentialsTempService.findPersonalData(requestDTO);
            return new ResponseEntity<>(personalDataCredentialDTO, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding personal data with message: {}");
            return new ResponseEntity<>(personalDataCredentialDTO, HttpStatus.FORBIDDEN);
        }
    }


}
