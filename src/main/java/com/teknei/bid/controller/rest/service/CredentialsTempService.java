package com.teknei.bid.controller.rest.service;

import com.teknei.bid.dto.*;
import com.teknei.bid.persistence.entities.BidDisp;
import com.teknei.bid.persistence.entities.CbidCredTemp;
import com.teknei.bid.persistence.entities.CbidEstaCred;
import com.teknei.bid.persistence.repository.BidDispRepository;
import com.teknei.bid.persistence.repository.CbidCredTempRepository;
import com.teknei.bid.persistence.repository.CbidEstaCredRepository;
import org.bouncycastle.asn1.x509.sigi.PersonalData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class CredentialsTempService {

    @Autowired
    private BidDispRepository bidDispRepository;
    @Autowired
    private CbidCredTempRepository credTempRepository;
    @Autowired
    private CbidEstaCredRepository estaCredRepository;
    private BCryptPasswordEncoder passwordEncoder;
    private static final String CRED_AVAILABLE_STATUS_NAME = "ACTIVO";
    private static final String CRED_USED_STATUS_NAME = "FINALIZADO";
    private static final String CRED_INACTIVE_STATUS_NAME = "INACTIVO";
    private Long idEstaCredAvailable;
    private Long idEstaCredUsed;
    private Long idEstaCredPause;
    private static final Logger log = LoggerFactory.getLogger(CredentialsTempService.class);
    private Map<String, Long> mapEstaDescId;
    private Map<Long, String> mapIdEstaDesc;

    @PostConstruct
    private void postConstruct() {
        initAvailableStatus();
        initUsedStatus();
        initPausedStatus();
        passwordEncoder = new BCryptPasswordEncoder();
        mapEstaDescId = new HashMap<>();
        mapIdEstaDesc = new HashMap<>();
        mapEstaDescId.put(CRED_AVAILABLE_STATUS_NAME, idEstaCredAvailable);
        mapEstaDescId.put(CRED_USED_STATUS_NAME, idEstaCredUsed);
        mapEstaDescId.put(CRED_INACTIVE_STATUS_NAME, idEstaCredPause);
        mapIdEstaDesc.put(idEstaCredAvailable, CRED_AVAILABLE_STATUS_NAME);
        mapIdEstaDesc.put(idEstaCredUsed, CRED_USED_STATUS_NAME);
        mapIdEstaDesc.put(idEstaCredPause, CRED_INACTIVE_STATUS_NAME);
    }


    public List<CbidCredTemp> findAll(){
        return credTempRepository.findAll();
    }

    public List<CbidCredTemp> findAllByIdEstaCred(Long idEstaCred){
        return credTempRepository.findAllByIdEstaCred(idEstaCred);
    }

    public Map<String, Long> findCredentialStatus(){
        return mapEstaDescId;
    }

    /**
     * @param requestDTO
     * @return 1- No matches for device serial, 2 - No matching temporal credentials available, 3 - Error disabling credentials ,0 = OK
     */
    public int deleteCredentials(CredentialsDeleteRequestDTO requestDTO) {
        List<BidDisp> dispList = bidDispRepository.findAllByNumSeriAndIdEsta(requestDTO.getSerial(), 1);
        if (dispList == null || dispList.isEmpty()) {
            return 1;
        }
        BidDisp disp = dispList.get(0);
        List<CbidCredTemp> listCredentials = credTempRepository.findAllByUsuaAndNoDactAndIdDispAndIdEstaCredAndIdEsta(requestDTO.getUsername(), requestDTO.getNoFingers(), disp.getIdDisp(), idEstaCredAvailable, 1);
        if (listCredentials == null || listCredentials.isEmpty()) {
            return 2;
        }
        if (listCredentials.size() > 1) {
            log.error("Multiple credentials for the same user are available, returning ok but invalidating remaining ones, must check business and db access. Incident will be reported");
        }
        try {
            listCredentials.forEach(c -> {
                c.setIdEstaCred(idEstaCredUsed);
                c.setIdEsta(2);
                c.setUsrOpeModi(requestDTO.getUsernameRequesting());
                c.setUsrModi(requestDTO.getUsernameRequesting());
                c.setFchModi(new Timestamp(System.currentTimeMillis()));
                credTempRepository.save(c);
            });
        } catch (Exception e) {
            log.error("Error disabling credentials in disable with message: {}", e.getMessage());
            return 3;
        }
        return 0;
    }

    /**
     * @param request
     * @return 1 - no device matches serial request, 2 - error saving credentials, 0 - OK
     */
    public int generateCredential(CredentialsSaveRequestDTO request) {
        List<BidDisp> dispList = bidDispRepository.findAllByNumSeriAndIdEsta(request.getSerial(), 1);
        if (dispList == null || dispList.isEmpty()) {
            return 1;
        }
        BidDisp disp = dispList.get(0);
        CbidCredTemp credTemp = new CbidCredTemp();
        credTemp.setUsua(request.getUsername());
        credTemp.setPass(request.getPassword());
        credTemp.setNomOpe(request.getName());
        credTemp.setApePate(request.getSurnameFirst());
        credTemp.setApeMate(request.getSurnameLast());
        credTemp.setNoDact(request.getNoFingers());
        credTemp.setIdDisp(disp.getIdDisp());
        credTemp.setIdEstaCred(idEstaCredAvailable);
        credTemp.setIdEsta(1);
        credTemp.setIdTipo(3);
        credTemp.setUsrCrea(request.getUserRequesting());
        credTemp.setFchCrea(new Timestamp(System.currentTimeMillis()));
        credTemp.setUsrOpeCrea(request.getUsername());
        try {
            credTempRepository.save(credTemp);
            return 0;
        } catch (Exception e) {
            log.error("Error saving temp credentials in database: {}", e.getMessage());
            return 2;
        }
    }

    private void initUsedStatus() {
        CbidEstaCred cbidEstaCred = estaCredRepository.findCbidEstaCredByCodEstaProc(CRED_USED_STATUS_NAME);
        if (cbidEstaCred == null) {
            log.error("No database configuration valid for status of used credentials, setting 3 as default");
            idEstaCredUsed = 3l;
        } else {
            idEstaCredUsed = cbidEstaCred.getIdEstaCred();
        }
    }

    private void initAvailableStatus() {
        CbidEstaCred cbidEstaCred = estaCredRepository.findCbidEstaCredByCodEstaProc(CRED_AVAILABLE_STATUS_NAME);
        if (cbidEstaCred == null) {
            log.error("No database configuration valid for status of available credentials, setting 1 as default");
            idEstaCredAvailable = 1l;
        } else {
            idEstaCredAvailable = cbidEstaCred.getIdEstaCred();
        }
    }

    private void initPausedStatus() {
        CbidEstaCred cbidEstaCred = estaCredRepository.findCbidEstaCredByCodEstaProc(CRED_INACTIVE_STATUS_NAME);
        if (cbidEstaCred == null) {
            log.error("No database configuration valid for status of paused credentials, setting 2 as default");
            idEstaCredPause = 2l;
        } else {
            idEstaCredPause = cbidEstaCred.getIdEstaCred();
        }
    }

    /**
     * @param request
     * @return 1 - fail for serial match, 2 - fail for username/password match 0- OK
     */
    public int validateCredentials(CredentialsValidationRequestDTO request) {
        log.info("info:buscando bid_disp findAllByNumSeri:"+request.getSerial()+"AndIdEsta:"+ 1);
        List<BidDisp> dispList = bidDispRepository.findAllByNumSeriAndIdEsta(request.getSerial(), 1);
        if (dispList == null || dispList.isEmpty()) {
            return 1;
        }
        BidDisp disp = dispList.get(0);
        log.info("Info:buscando cbid_cred_temp findAllByUsua: " + request.getUsername() + " AndIdDisp:"
				+ disp.getIdDisp() + " AndIdEstaCred:" + idEstaCredAvailable + " AndIdEsta:" + 1);
        List<CbidCredTemp> listCredentials = credTempRepository.findAllByUsuaAndIdDispAndIdEstaCredAndIdEsta(request.getUsername(), disp.getIdDisp(), idEstaCredAvailable, 1);
        if (listCredentials == null || listCredentials.isEmpty()) {
            return 2;
        }
        boolean passwordMatch = passwordEncoder.matches(request.getPassword(), listCredentials.get(0).getPass());
        String requestPassword = passwordEncoder.encode(request.getPassword());
        log.info("request.getPassword() "+ request.getPassword());
        log.info("request.getPassword().Encode(): "+ requestPassword);
         String testpassword = passwordEncoder.encode(request.getUserRequesting());
        log.info("*testpassword:"+request.getUserRequesting());
        log.info("*testpassword.encode:"+testpassword);       
        boolean  passwordMatch2 =  passwordEncoder.matches(request.getUserRequesting(), testpassword);        
        log.info("passwordMatch2: "+passwordMatch2);
        
        
        log.info("request.getPassword() "+ request.getPassword());
        log.info("listCredentials.get(0).getPass(): "+ listCredentials.get(0).getPass());
        log.info("passwordMatch: "+passwordMatch);
       

        if (!passwordMatch) {
            return 2;
        }
        if (listCredentials.size() > 1) {
            log.error("Multiple credentials for the same user are available, returning ok but invalidating remaining ones, must check business and db access. Incident will be reported");
        }
        try {
            listCredentials.forEach(c -> {
                c.setIdEstaCred(idEstaCredUsed);
                c.setIdEsta(2);
                c.setUsrOpeModi(request.getUserRequesting());
                c.setUsrModi(request.getUserRequesting());
                c.setFchModi(new Timestamp(System.currentTimeMillis()));
                credTempRepository.save(c);
            });
        } catch (Exception e) {
            log.error("Error updating credentials in disable with message: {}", e.getMessage());
        }
        return 0;
    }

    public PersonalDataCredentialDTO findPersonalData(CredentialsValidationRequestDTO request){
        List<BidDisp> dispList = bidDispRepository.findAllByNumSeriAndIdEsta(request.getSerial(), 1);
        if (dispList == null || dispList.isEmpty()) {
            throw new IllegalArgumentException("1");
        }
        BidDisp disp = dispList.get(0);
        List<CbidCredTemp> listCredentials = credTempRepository.findAllByUsuaAndIdDisp(request.getUsername(), disp.getIdDisp());
        if (listCredentials == null || listCredentials.isEmpty()) {
            throw new IllegalArgumentException("2");
        }
        boolean passwordMatch = passwordEncoder.matches(request.getPassword(), listCredentials.get(0).getPass());
        if (!passwordMatch) {
            throw new IllegalArgumentException("2");
        }
        if (listCredentials.size() > 1) {
            log.error("Multiple credentials for the same user are available, returning ok but invalidating remaining ones, must check business and db access. Incident will be reported");
            throw new IllegalArgumentException("3");
        }
        CbidCredTemp credTemp = listCredentials.get(0);
        PersonalDataCredentialDTO personalDataCredentialDTO = new PersonalDataCredentialDTO();
        personalDataCredentialDTO.setLastnameFirst(credTemp.getApePate());
        personalDataCredentialDTO.setLastnameLast(credTemp.getApeMate());
        personalDataCredentialDTO.setName(credTemp.getNomOpe());
        personalDataCredentialDTO.setStatus(0);
        return personalDataCredentialDTO;
    }

    public int validateNoFingersByCredentials(CredentialsValidationFingersRequestDTO request) throws IllegalArgumentException {
        List<BidDisp> dispList = bidDispRepository.findAllByNumSeriAndIdEsta(request.getSerial(), 1);
        if (dispList == null || dispList.isEmpty()) {
            throw new IllegalArgumentException("1");
        }
        BidDisp disp = dispList.get(0);
        List<CbidCredTemp> listCredentials = credTempRepository.findAllByUsuaAndIdDisp(request.getUsername(), disp.getIdDisp());
        if (listCredentials == null || listCredentials.isEmpty()) {
            throw new IllegalArgumentException("2");
        }
        boolean passwordMatch = passwordEncoder.matches(request.getPassword(), listCredentials.get(0).getPass());
        if (!passwordMatch) {
            throw new IllegalArgumentException("2");
        }
        if (listCredentials.size() > 1) {
            log.error("Multiple credentials for the same user are available, returning ok but invalidating remaining ones, must check business and db access. Incident will be reported");
            throw new IllegalArgumentException("3");
        }
        CbidCredTemp credTemp = listCredentials.get(0);
        return credTemp.getNoDact();
    }

    public CredentialsTempDTO entityToDTO(CbidCredTemp cbidCredTemp){
        CredentialsTempDTO dto = new CredentialsTempDTO();
        dto.setApeMate(cbidCredTemp.getApeMate());
        dto.setApePate(cbidCredTemp.getApePate());
        dto.setFchCrea(cbidCredTemp.getFchCrea());
        dto.setIdCredTemp(cbidCredTemp.getIdCredTemp());
        dto.setIdDisp(cbidCredTemp.getIdDisp());
        dto.setIdEstaCred(cbidCredTemp.getIdEstaCred());
        dto.setNoDact(cbidCredTemp.getNoDact());
        dto.setUsua(cbidCredTemp.getUsua());
        dto.setNomOpe(cbidCredTemp.getNomOpe());
        dto.setSerial(bidDispRepository.findOne(cbidCredTemp.getIdDisp()).getNumSeri());
        dto.setEstaCredDsc(mapIdEstaDesc.get(cbidCredTemp.getIdEstaCred()));
        return dto;
    }

    public boolean updateCredStatus(UpdateTempStatusCredRequestDTO requestDTO){
        //TODO search for serial
        BidDisp disp = null;
        List<BidDisp> dispList = bidDispRepository.findAllByNumSeriAndIdEsta(requestDTO.getSerial(), 1);
        if (dispList == null || dispList.isEmpty()) {
            log.warn("No serial relation found: {}", requestDTO.getSerial());
        }else{
            disp = dispList.get(0);
        }
        CbidCredTemp credTemp = credTempRepository.findOne(requestDTO.getIdCredTemp());
        if(credTemp == null){
            return false;
        }
        credTemp.setIdEstaCred(requestDTO.getIdEstaCred());
        credTemp.setFchModi(new Timestamp(System.currentTimeMillis()));
        credTemp.setUsrModi(requestDTO.getUserReq());
        credTemp.setUsrOpeModi(requestDTO.getUserReq());
        if(disp != null){
            credTemp.setIdDisp(disp.getIdDisp());
        }
        try {
            credTempRepository.save(credTemp);
            return true;
        } catch (Exception e) {
            log.error("Error updating temporal credential with message: {}", e.getMessage());
            return false;
        }
    }

}
